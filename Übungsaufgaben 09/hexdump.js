/* eslint-disable no-process-exit, no-sync */
'use strict';

const fs = require('fs');

if (process.argv.length != 3) {
  process.stdout.write('Usage: hexdump <dateiname>\n');
  process.exit(1);
}

if (!fs.existsSync(process.argv[2]) || fs.statSync(process.argv[2]).isDirectory()) {
  process.stdout.write(`No such file: ${process.argv[2]}\n`);
  process.exit(2);
}

let position = 0;
const fileLength = fs.statSync(process.argv[2]).size;

try {
  const input = fs.readFileSync(process.argv[2]);
  while (position < fileLength) {
    const charsRead = input.slice(position, position + 16);
    if (charsRead && charsRead.length > 0) {
      process.stdout.write(`${position.toString().padStart(4, '0')}: `);
      position += charsRead.length;

      for (let i = 0; i < 16; i++) {
        if (i < charsRead.length) {
          const hex = `${charsRead[i].toString(16).padStart(2, '0')}`;
          process.stdout.write(`${hex} `);
        } else {
          process.stdout.write('  ');
        }

        if (i == 7) {
          process.stdout.write('-- ');
        }

        if (charsRead[i] < 32 || charsRead[i] > 250) {
          charsRead[i] = '.'.charCodeAt(0);
        }
      }

      const bufferContent = charsRead.toString('ascii');
      process.stdout.write(`  ${bufferContent}\n`);
    }
  }
} catch (err) {
  process.stdout.write(`${err.toString()}\n`);
}
