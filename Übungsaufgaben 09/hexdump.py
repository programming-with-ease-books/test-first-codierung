import pathlib
import sys

if len(sys.argv) != 2:
    print("Usage: hexdump.py <dateiname>\n")
    sys.exit(1)

if not pathlib.Path(sys.argv[1]).exists() or pathlib.Path(sys.argv[1]).is_dir():
    print(f"No such file: {sys.argv[1]}")
    sys.exit(2)

position = 0
file_length = pathlib.Path(sys.argv[1]).stat().st_size

try:
    file = open(sys.argv[1], "rb")
    input_ = bytearray(file.read())
    file.close()
    while position < file_length:
        chars_read = input_[position:position + 16]
        if chars_read and len(chars_read) > 0:
            sys.stdout.write(f'{str(position).rjust(4, "0")}: ')
            position += len(chars_read)

            for i in range(16):
                if i < len(chars_read):
                    hex_ = f'{bytes([chars_read[i]]).hex()}'
                    sys.stdout.write(f"{hex_} ")
                else:
                    sys.stdout.write("  ")

                if i == 7:
                    sys.stdout.write("-- ")

                if i < len(chars_read) and ((chars_read[i] < 32) or (chars_read[i] > 250)):
                    chars_read[i] = ord(".")

            buffer_content = bytes(c % 128 for c in chars_read).decode("ascii")
            sys.stdout.write(f"  {buffer_content}\n")

except Exception as err:
    sys.stdout.write(f"{str(err)}\n")
